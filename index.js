JiraApi = require('jira').JiraApi;
_ = require('underscore');
async = require('async');
prompt = require('prompt');

config = { "host":"jirachad.icehealthsystems.com","user":"dng","password":"E^RSi155#D9Fg&Q3LQ04C6WE"  };
jiraRecord = {};

var promptschema = { "properties": {
  "account": {
      "description": 'Enter Account JIRA',
      "required": true
  },
    "vpcstack": {
        "description": 'Enter VPC Stack name',
        "required": true
    },
    "appstack": {
        "description": 'Enter Application Stack name',
        "required": true
    },
    "data-asset": {
        "description": 'Enter Data Asset name',
        "required": true
    },
  }
};

var promptloginschema = { "properties": {
    "username": {
        "description": 'Enter Jira username',
        "required": true
    },
    "password": {
      "hidden": true
    }
  }
};

var login = function (callback) {
  prompt.start();
  prompt.get(promptloginschema, function (err, result) {
    config.username = result.username;
//    config.password = result.password;
    callback();
  })
}

var jiraPromptInput = function (callback) {
  prompt.start();
  prompt.get(promptschema, function (err, result) {
//    config.username = result.username;
//    config.password = result.password;
    jiraRecord=result;
    callback();
  })
}
var jira = new JiraApi('https', config.host, config.port, config.user, config.password, '2');

if ( process.argv.length < 2 ) {
	console.log("node index <KEY>");
	process.exit(-1);
}

var stackname = process.argv[ 2 ];

var CAissue = {
  "fields": {
    "project": {
      "key": "ST"
    },
    "summary": stackname,
    "description": "testing out posting with nodejs",
    "issuetype": {
      "name": "ST:Cloud Asset"
       }
   }
}

var addJIRA = function() {
  jira.addNewIssue( CAissue, function(error, response) {
    if (error) {
      console.log(error);
    } else {
      console.log('Status: ' + JSON.stringify(response));
      var CAlabel = { "update" : {
        "labels" : [
            {"add" : "app-stack"}
        ]
      }};
      jira.updateIssue(response.key , CAlabel, function(error, response) {
        if (error) {
          console.log(error);
        } else {
          console.log('Status: ' + JSON.stringify(response.id));
        };
      });
    };
  });
}

var main = function() {
  login( function(err, response) {
    jiraPromptInput( function(err, response) {
      console.log(JSON.stringify(jiraRecord));
    });
  });
}

main();
